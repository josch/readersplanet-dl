#!/bin/env python
# -*- coding: utf-8 -*-

import os

for root, dirs, files in os.walk('txt'):
	for filename in files:
		oldpath = os.path.join(root, filename)
		f = open(oldpath)
		firstline = f.readline()
		f.close()
		if firstline.startswith("\fD I E G R Ö S S T E S C I E N C E - F I C T I O N - S E R I E D E R W E L T"):
			newpath = os.path.join("diegrößtesciencefictionseriederwelt2", filename)
			os.rename(oldpath, newpath)
