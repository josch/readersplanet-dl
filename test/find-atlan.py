#!/bin/env python

import os

for root, dirs, files in os.walk('txt'):
	for filename in files:
		oldpath = os.path.join(root, filename)
		f = open(oldpath)
		firstline = f.readline()
		f.close()
		if firstline.startswith("ATLAN") or firstline.startswith("\fATLAN"):
			newpath = os.path.join("atlan", filename)
			os.rename(oldpath, newpath)
