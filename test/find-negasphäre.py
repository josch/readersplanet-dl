#!/bin/env python
# -*- coding: utf-8 -*-

import os

for root, dirs, files in os.walk('txt'):
	for filename in files:
		oldpath = os.path.join(root, filename)
		f = open(oldpath)
		firstline = f.readline()
		f.close()
		if firstline.startswith("\fN e g a s p h ä r e"):
			newpath = os.path.join("negasphäre", filename)
			os.rename(oldpath, newpath)
