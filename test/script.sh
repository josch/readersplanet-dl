# for older ebooks:
# pdftoppm -f 4 -l 4 -r 220 -x 164 -y 242 -W 1490 -H 2166 -png pdf/528.pdf foo

pdftoppm -f 3      -r 190 -x 164 -y 278 -W 1490 -H 2130 -png pdf/528.pdf foo
mogrify -gamma 0.25 -resize 600x800! +dither -type Grayscale -colors 8 foo-*.png

pdftoppm -f 1 -l 1 -r 300 -png pdf/528.pdf foo
mogrify -resize 600x800 -type Grayscale foo-01.png

pdftoppm -f 2 -l 2 -r 300 -x 164 -y 278 -W 1490 -H 2130 -png pdf/528.pdf foo
mogrify -resize 600x800 -type Grayscale foo-02.png
