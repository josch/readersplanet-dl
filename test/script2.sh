#!/bin/sh

#time gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/screen -dNOPAUSE -dQUIET -dBATCH -sOutputFile=output.pdf 064\ -\ Clark\ Darlton\ -\ Perry\ Rhodan\ -\ 64\ -\ Im\ Zeit-Gefängnis.pdf
#root@192.168.111.1:/media/sd/PerryRhodan/Perry\\\ Rhodan\\\ -\\\ Romanzyklus\\\ -\\\ 0050-0099\\\ -\\\ Atlan\\\ und\\\ Arkon/

[ $# = 2 ] || { echo "usage: $0 input.pdf output.pdf"; exit 1; }

INPUT=$1
OUTPUT=$2

LEFT=0
TOP=-30
RIGHT=0
BOTTOM=-20

PART1=`mktemp`
PART2=`mktemp`
PART3=`mktemp`
PART4=`mktemp`
PART5=`mktemp`

pdftk $INPUT cat 1-3 output $PART1
pdfcrop $PART1 $PART3
pdftk $INPUT cat 4-end output $PART2
pdfcrop --margins "$LEFT $TOP $RIGHT $BOTTOM" --clip $PART2 $PART4
pdftk $PART3 $PART4 cat output $PART5
pdfcrop $PART5 "$OUTPUT"

rm $PART1 $PART2 $PART3 $PART4 $PART5
