#!/bin/env python

import os

for root, dirs, files in os.walk('txt'):
	for filename in files:
		oldpath = os.path.join(root, filename)
		f = open(oldpath)
		firstline = f.readline()
		f.close()
		if firstline.startswith("\fNr. "):
			newpath = os.path.join("perry", filename)
			os.rename(oldpath, newpath)
