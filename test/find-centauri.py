#!/bin/env python

import os

for root, dirs, files in os.walk('txt'):
	for filename in files:
		oldpath = os.path.join(root, filename)
		f = open(oldpath)
		firstline = f.readline()
		f.close()
		if firstline.startswith("\fCentauri-Zyklus"):
			newpath = os.path.join("centauri", filename)
			os.rename(oldpath, newpath)
