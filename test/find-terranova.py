#!/bin/env python

import os

for root, dirs, files in os.walk('txt'):
	for filename in files:
		oldpath = os.path.join(root, filename)
		f = open(oldpath)
		firstline = f.readline()
		f.close()
		if firstline.startswith("\fT E R R A N O V A"):
			newpath = os.path.join("terranova", filename)
			os.rename(oldpath, newpath)
